import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture_url(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(f'https://api.pexels.com/v1/search?query={city} {state}&per_page=1', headers=headers)
    data = response.json()
    picture_url = data['photos'][0]['src']['original']
    return picture_url


def get_weather(city, state):
    geo_url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}'
    geo_response = requests.get(geo_url)
    geo_data = geo_response.json()
    lat = geo_data[0]['lat']
    lon = geo_data[0]['lon']
    weather_url = f'https://api.openweathermap.org/data/3.0/onecall?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}'
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()
    temp = weather_data['current']['temp']
    description = weather_data['current']['weather'][0]['description']
    return {'temp': temp, 'description': description}
